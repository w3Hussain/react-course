import React from 'react'
import axios from 'axios'
import { Alert } from 'bootstrap'
function Api() {
        
    const data = () =>{
        axios.get("https://dummyjson.com/products/1").then((resp)=>{
        console.log(resp.data)
        alert("Fetched Successfully")
    }).catch(error=>console.error("Error occured"+error))
    }

  return (
    <div>
        <h1>Fetch Api</h1>
        <button onClick={data}>Fetch</button>
    </div>
  )
}

export default Api