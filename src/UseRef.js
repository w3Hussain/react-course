import React,{useRef} from 'react'

function UseRef() {
    let inputRef = useRef(null)
    function handleinput(){
        console.log("Hello")
        inputRef.current.value="100"+"Hello"
        inputRef.current.focus();
    }
  return (
   <div>
       <h1>Use Ref Hook</h1>
       <input type="text" ref={inputRef}/>
       <button onClick={handleinput}>HandleInput</button>
   </div>
  )
}

export default UseRef