import React,{useState} from 'react'
import './App.css'
// import ConditionalRendering from './ConditionalRendering'
// import User from './User'
// import Form from './Form'
// import FunctionProps from './FunctionProps'
// import NewForm from './NewForm'
// import UseEffect from './UseEffect'
// import ReactBootStrap from './ReactBootStrap'
// import NestedList from './NestesList'
// import ReactFragment from './ReactFragment'
// import Child from './Child' 
// import UseMemo  from './UseMemo'
// import UseRef from './UseRef'
// import Ab from './Ab'
// import Home from './components/Home'
// import {BrowserRouter,Routes,Route,Link,Navigate} from 'react-router-dom'
// import About from './components/About'
// import NavBar from './components/NavBar'
// import Page404 from './components/Page404'
// import DynamicRouteParams from './components/DynamicRouteParams'
// import User from './User'
// import Api from './Api'
// import MainHome from './components/MainHome'
import HomeComponent from './components/MainHome'
const App = () => {
  // const [count, setcount] = useState(10)
  // const [data,setData] = useState(100)
  // function alert1 (data){
  //   alert("Hello world"+data)
  // }
  return (
    <div className='App'>
      {/* <User name={"abc"} email={"h@gmail.com"} other={{mobile:"1122",address:"delhi"}}/> */}
      {/* <Ab/> */}
      {/* <Form/> */}
      {/* <ConditionalRendering no={5}/> */}
      {/* <NewForm/> */}
      {/* <FunctionProps data={fun}/> */}
      {/* REACT HOOKS */}

      {/* <h1>Use Effect{count}</h1><br /><br />
      <h1>Update data{data}</h1> */}
      {/* <UseEffect count={count} data={data}/>
      <button onClick={()=>{setcount(count+1)}}>Update</button><br /><br />
      <button onClick={()=>{setData(data+1)}}>Data</button> */}

      {/* ?\REACT BOOTSTRAP */}
      {/* <ReactBootStrap/> */}
      {/* <NestedList/> */}
      {/* <ReactFragment/> */}
      {/* <Child alert={alert1}/> */}
      {/* <UseMemo/> */}
      {/* <UseRef/> */}
      {/* <NavBar/> */}
      {/* <BrowserRouter> */}
      {/* <Link to="/about">About</Link> */}
      {/* <NavBar/> */}
      {/* <DynamicRouteParams/>
      <Routes>
        <Route path="/" element={<Home/>}></Route>
        <Route path="/about" element={<About/>}></Route>
        <Route path="/user" element={<User/>}></Route> */}
        {/* <Route path="/*" element={<Navigate to="/home"></Navigate>}></Route> */}
      {/* </Routes> */}
      {/* <Page404/> */}
      {/* </BrowserRouter> */}
      {/* <Api/> */}
      {/* <MainHome/> */}
      <HomeComponent/>
    </div>
  )
}

export default App