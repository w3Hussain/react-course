import React,{useState} from 'react'

function User(props) {  

    const [name, setname] = useState("")
    const [print, setprint] = useState(false)
  return (  
      <>
    
      <input type="input" onChange={(e)=>{
        //   console.log(e.target.value)
          setname(e.target.value)
      }}/>
      {
         print?
         <h1>{name}</h1>:null
      }
      <button onClick={()=>{setprint(true)}}>submit</button>
      </>
    
  )
}

export default User