import React,{useState,useEffect} from 'react'

function UseEffect(props) {

  useEffect(() => {
    console.warn("call")
  },[props.count])
  return (
    <div>
    <h1>Count Props:{props.count}</h1>
    <h1>Data Props:{props.data}</h1>
    
    </div>
  )
}

export default UseEffect