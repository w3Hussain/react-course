import React,{useState} from 'react'

function ConditionalRendering(props) {
    const [logged, setlogged] = useState(props.no)
    const log = 2;
  return (
    <div>
        {log==1?<h1>Welcome User 1</h1>:log==2?<h1>Welcome user 2</h1>:<h1>Welcome Guest</h1>}
    </div>
  )
}

export default ConditionalRendering