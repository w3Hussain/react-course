import React from 'react'

function FunctionProps(props) {
  return (
    <div>
        <h1>Function Props</h1>
        <button onClick={props.data}>GetData</button>
    </div>
  )
}

export default FunctionProps