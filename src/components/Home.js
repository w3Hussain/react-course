import React from 'react'
import { useLocation } from 'react-router-dom'

function Home() {
  const location = useLocation();
  console.log(location)
  return (
    <div>
      <h1>Testing page</h1>
      <p>Hello</p>
    </div>
  )
}

export default Home