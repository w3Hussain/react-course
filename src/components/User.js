import React from 'react'

function User() {
  return (
    <div> 
        <ul>
            <li><Link to="/about"></Link></li>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/user/u1">u1</Link></li>
            <li><Link to="/user">u2</Link></li>
        </ul>
    </div>
  )
}

export default User