import React, { useState,useMemo } from "react";

function UseMemo() {
  const [count, setcount] = useState(0);
  const [item, setItem] = useState(10);
    const multicountMemo = useMemo(function multicount(){
        console.warn("multcount")
        return count*5
    }) 
  return (
    <>
      <h1>useMemo {count}</h1>
      <h1>setItem:{item}</h1>
      <h1>{multicountMemo}</h1>
      <button onClick={() => setcount(count + 1)}>Click me</button>
      <button onClick={()=> setItem(item*10)}>SetItem</button>
    </>
  );
}

export default UseMemo;
