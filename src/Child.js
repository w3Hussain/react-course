import React from 'react'

function Child(props) {
    const data = "ABC";
  return (
    <div>
        <h2>Hello</h2>
        <button onClick={()=>props.alert(data)}>Click Me</button>
    </div>
  )
}

export default Child