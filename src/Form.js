import React,{useState} from 'react'

function Form() {
    const [name, setName] = useState("")
    const [select, setSelect] = useState("")
    const [tnc, setTnc] = useState(false)
    const getdata =(e)=>{
        console.log(name,select,tnc)
        e.preventDefault();

    }
  return (
    <>
    <form onSubmit={getdata}>
    <input type="text" placeholder='enter name' onChange={(e)=>{setName(e.target.value)}}/><br /><br />
    <select onChange={(e)=>{setSelect(e.target.value)}}>
        <option >Car</option>
        <option >bus</option>
        <option >bike</option>
        <option >train</option>
    </select><br /><br />
    <input type="checkbox"  onChange={(e)=>{setTnc(e.target.checked)}} /><span>Accept TNC</span>
    <button>Submit</button>
    </form>
    </>
  )
}

export default Form