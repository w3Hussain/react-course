import React,{useState} from 'react'

function Ab() {
    const [status, setStatus] = useState(true)
  return (
    <>
    {
        status?
        <h1>Hello World</h1>:null
    }
    <button onClick={()=>{setStatus(!status)}}>Toggle</button>
    {/* <button onClick={()=>{setStatus(false)}}>Hide</button> */}
    

    </>
  )
}

export default Ab